package exercises.kantor;

import javax.json.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CurrencyJsonReader {
  static private String url = "http://api.nbp.pl/api/exchangerates/tables/A?format=json";
  private URL urlToAPI;

  //zdefiniujemy klase wewn, ktore nie bedzie widoczna na zewn(moze?)
  class Currencyrate {
    String name;
    double rate;
    String code;

    public Currencyrate(String name, double rate, String code) {
      this.name = name;
      this.rate = rate;
      this.code = code;
    }

    @Override
    public String toString() {
      return "Currencyrate{" +
              "name='" + name + '\'' +
              ", rate=" + rate +
              ", code='" + code + '\'' +
              '}';
    }
    }
    public CurrencyJsonReader() throws MalformedURLException {
      urlToAPI = new URL(url);
    }
    //metoda ktora pobiera interesujace nas dane
    public List<Currencyrate> getCurrencies() throws IOException {
      List<Currencyrate> list = new ArrayList<>();

      if (urlToAPI != null) {
        //tworzymy fabryke Readera
        JsonReaderFactory readerFactory = Json.createReaderFactory(Collections.emptyMap());
        //robimy samego readera
        JsonReader jsonReader = readerFactory.createReader(urlToAPI.openStream());
        //tworzymy json tablice
        JsonArray jsonObject = jsonReader.readArray();
        JsonArray rates = jsonObject.getJsonObject(0).getJsonArray("rates");
        //w ktorej mamy dokladnie 1 obiekt
        //szukamy pola o nazwie rates
        //System.out.println(jsonObject.getJsonObject(0).getJsonArray("rates"));
        rates.forEach(item -> {
          String cur = item.asJsonObject().getString("currency");
          JsonNumber rate = item.asJsonObject().getJsonNumber("mid");
          String code = item.asJsonObject().getString("code");
          Currencyrate currencyrate = new Currencyrate(cur, rate.doubleValue(), code);
          list.add(currencyrate);
        });
      }
      return list;
    }
  }

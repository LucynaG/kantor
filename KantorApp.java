package exercises.kantor;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class KantorApp extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {

    VBox root = new VBox();
    root.setAlignment(Pos.CENTER);
    root.setSpacing(10);
    root.setPadding(new Insets(10, 10, 10, 10));

    Scene scene = new Scene(root, 300, 300);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Kantor walut");
    primaryStage.show();

    List<CurrencyJsonReader.Currencyrate> list;
    List<CurrencyJsonReader.Currencyrate> temp = new ArrayList<>();

    Label comboLabel = new Label("Wybierz walute");
    root.getChildren().addAll(comboLabel);
    ComboBox<String> waluta = new ComboBox<>();
    try {
      CurrencyJsonReader reader = new CurrencyJsonReader();
      temp = reader.getCurrencies();

      for (CurrencyJsonReader.Currencyrate item : temp) {
        waluta.getItems().add(item.name+" "+item.rate+" "+item.code);
      }
      waluta.getSelectionModel().select(0);
    } catch (MalformedURLException e) {
      Alert info = new Alert(Alert.AlertType.ERROR);
      info.setHeaderText("Niepoprawny format url do API");
      info.setContentText("Nie można pobrać kursów walut");
      info.show();
    } catch (IOException e) {
      Alert info = new Alert(Alert.AlertType.ERROR);
      info.setHeaderText("Błąd sieci");
      info.setContentText("Nie można pobrać kursów walut");
      info.show();
    } finally {
      list = temp;
    }

    root.getChildren().add(waluta);

    Label kursLabel = new Label("KURSY");
    kursLabel.setAlignment(Pos.BASELINE_LEFT);
    root.getChildren().add(kursLabel);


    Label kwotaZLLabel = new Label("Kwota w zł:");
    kwotaZLLabel.setAlignment(Pos.CENTER);
    root.getChildren().add(kwotaZLLabel);
    TextField kwota = new TextField();
    kwota.setAlignment(Pos.CENTER);
    kwota.setOnKeyPressed(event -> {

    });
    root.getChildren().add(kwota);

    Label kwotaWalutaLabel = new Label("Kwota w walucie:");
    kwotaWalutaLabel.setAlignment(Pos.CENTER);
    root.getChildren().add(kwotaWalutaLabel);
    TextField kwotaWaluta = new TextField();
    kwotaWaluta.setAlignment(Pos.CENTER);
    kwotaWaluta.setEditable(false);
    root.getChildren().add(kwotaWaluta);

    kwota.setOnAction(event -> {
      //if(kwota.getText().equals("") && waluta.getSelectionModel().getSelectedIndex()> -1) {
      //pobranie indeksu pozycji wybranej z listy walut
      int index = waluta.getSelectionModel().getSelectedIndex();
      //pobranie rekordu z listy kursów
      CurrencyJsonReader.Currencyrate rateNBP = list.get(index);
      //konwersja pola tekst. z kwotą na typ double
      double kwotaPoKonwersji = Double.parseDouble(kwota.getText());
      //obliczanie kwoty w wybranej walucie i umieszczenie w kontrolce "kwota w walucie"
      kwotaWaluta.setText((rateNBP.rate * kwotaPoKonwersji) + "");

    });
  }
}

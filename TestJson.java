package exercises.kantor;

import java.io.IOException;
import java.util.List;

public class TestJson {
  public static void main(String[] args) throws IOException {
    CurrencyJsonReader reader = new CurrencyJsonReader();
    List<CurrencyJsonReader.Currencyrate> lista = reader.getCurrencies();
    for (CurrencyJsonReader.Currencyrate item: lista) {
      System.out.println(item);
    }
  }
}
